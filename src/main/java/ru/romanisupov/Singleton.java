package ru.romanisupov;

public final class Singleton {
    private static Singleton instance;

    private Singleton() { }

    // Я выбрал этот подход, поскольку инициализация в статическом блоке гарантирует создание экземпляра
    // в начале вполнения программы. Это поможет разрешить проблемы, связанные с многопоточностью, поскольку класс
    // теперь заведомо является потокобезопасным.
    // Основные проблемы здесь:
    // 1) Неудобство в тестировании
    // 2) Невозможность повторно использовать класс
    // 3) Перегруженность класса (класс отвечает не только за свое поведение, но и за свою единственность.)
    static {
        try {
            instance = new Singleton();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Singleton getInstance() {
        return instance;
    }
}
